FROM ubuntu:20.04

LABEL maintainer="Morteza"

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Tehran
ARG _UID
ARG _GID
ARG SWIFT_VERSION

RUN apt-get update \
    && apt-get install -y \
    build-essential \
    curl \   
    git \
    liberasurecode-dev \
    libffi-dev \
    libssl-dev \
    memcached \
    python3-dev \
    python3-pip \
    rsync \
    sqlite3 \
    xfsprogs \
    net-tools \
    nano \
    pwgen \
    rsyslog \
    supervisor \
    iputils-ping \
    && groupadd -g "${_GID}" swift \
    && useradd --create-home --no-log-init -u "${_UID}" -g "${_GID}" swift \
    && python3 -m pip install --upgrade pip \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && apt-get clean

RUN python3 -m pip install python-swiftclient python-keystoneclient keystonemiddleware swift==${SWIFT_VERSION}

COPY rootfs/ /

RUN mkdir -p /var/cache/swift /var/lock/swift /var/run/swift \ 
    && chown -R root:swift /etc/swift /var/cache/swift /var/lock/swift /var/run/swift \
    && chmod -R 775 /var/cache/swift \
    && mkdir -p /var/log/supervisor \
    && mkdir -p /srv/node \
    && chown -R swift:swift /srv

EXPOSE 8080

RUN chmod 700 /usr/local/bin/init.sh

CMD /usr/local/bin/init.sh
